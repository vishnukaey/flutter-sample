import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kitchenkart/Models.dart';
import 'package:flutter/cupertino.dart';

void main() => runApp(new Kitchen());

final ThemeData _themeData = new ThemeData(
  primaryColor: Colors.black,
);

class Kitchen extends StatefulWidget {
  @override
  _KitchenState createState() {
    return new _KitchenState();
  }
}

class _KitchenState extends State<Kitchen> {
  @override
  Widget build(BuildContext context) {
    var _routes = <String, WidgetBuilder>{
      "/detail": (BuildContext context) => new DetailPage(),
      // Add another page
    };
    return new MaterialApp(
      title: "KitchenKart",
      theme: _themeData,
      home: new HomePage(),
      routes: _routes,
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Drawer drawer;

  @override
  Widget build(BuildContext context) {
    drawer = new Drawer(
      child: new ListView.builder(
        padding: new EdgeInsets.only(top: 100.0, left: 20.0),
        itemExtent: 50.0,
        itemBuilder: (BuildContext context, int index) {
          return new Container(
            child: new FlatButton(
              onPressed: _itemselected,
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new Icon(Icons.star_border, color: Colors.black),
                  new Text('   Entry $index'),
                ],
              ),
            ),
          );
        },
      ),
    );

    return new Scaffold(
      drawer: drawer,
      appBar: new AppBar(
        title: new Text("Home"),
        actions: <Widget>[
          new IconButton(
            icon: const Icon(Icons.search),
            tooltip: 'Search',
            onPressed: searchTapped,
          ),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        child: const Icon(Icons.edit),
        onPressed: () {
          showDemoDialog<String>(
            context: context,
            child: new CupertinoAlertDialog(
                title: const Text(
                    'Allow "Maps" to access your location while you use the app?'),
                content: const Text(
                    'Your current location will be displayed on the map and used for directions, '
                    'nearby search results, and estimated travel times.'),
                actions: <Widget>[
                  new CupertinoDialogAction(
                      child: const Text('Don\'t Allow'),
                      onPressed: () {
                        Navigator.pop(context, 'Disallow');
                      }),
                  new CupertinoDialogAction(
                      child: const Text('Allow'),
                      onPressed: () {
                        Navigator.pop(context, 'Allow');
                      }),
                ]),
          );
        },
      ),
      body: new Center(
        child: buildGrid(),
      ),
    );
  }

  List<Container> _buildGridTileList(int count) {
    return new List<Container>.generate(
        count,
        (int index) => new Container(
                child: new FlatButton(
              child: new Image.asset('lib/apple.jpg'),
              onPressed: _onPressed,
            )));
  }

  Widget buildGrid() {
    return new GridView.extent(
        maxCrossAxisExtent: 150.0,
        padding: const EdgeInsets.all(4.0),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        children: _buildGridTileList(30));
  }

  void _onPressed() {
    Navigator.of(context).pushNamed("/detail");
  }

  void _itemselected() {
    print('Selected');
  }

  void searchTapped() {
    print('Search');
  }

  void showDemoDialog<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      child: child,
      barrierDismissible: false,
    ).then<Null>((T value) {
      // The value passed to Navigator.pop() or null.
      if (value != null) {}
    });
  }
}

class DetailPage extends StatefulWidget {
  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Kitchen"),
      ),
      body: new RefreshIndicator(
          child: new ListView.builder(itemBuilder: _itemBuilder),
          onRefresh: _onRefresh),
    );
  }

  Future<Null> _onRefresh() {
    Completer<Null> completer = new Completer<Null>();
    Timer timer = new Timer(new Duration(seconds: 3), () {
      completer.complete();
    });
    return completer.future;
  }

  Widget _itemBuilder(BuildContext context, int index) {
    Item item = getItem(index);
    return new detailItemWidget(item: item);
  }

  Item getItem(int index) {
    return new Item(false, "Kitchen Property $index");
  }
}

class detailItemWidget extends StatefulWidget {
  detailItemWidget({Key key, this.item}) : super(key: key);
  final Item item;

  @override
  _detailItemWidgetState createState() => new _detailItemWidgetState();
}

class _detailItemWidgetState extends State<detailItemWidget> {
  @override
  Widget build(BuildContext context) {
    return new ListTile(
      leading: new Icon(Icons.add_box),
      title: new Text(widget.item.name),
      onTap: _onTap,
    );
  }

  void _onTap() {
    Route route = new MaterialPageRoute(
      settings: new RouteSettings(name: "/detail/item"),
      builder: (BuildContext context) => new KitchenItemPage(item: widget.item),
    );
    Navigator.of(context).push(route);
  }
}

class KitchenItemPage extends StatefulWidget {
  KitchenItemPage({Key key, this.item}) : super(key: key);
  final Item item;

  @override
  _KitchenItemPageState createState() => new _KitchenItemPageState();
}

class _KitchenItemPageState extends State<KitchenItemPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Kitchen Property"),
      ),
      body: new Container(
        padding: const EdgeInsets.all(32.0),
        child: new Row(
          children: [
            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: new Text(
                      'Kitchen Product',
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  new Container(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: new Image.asset(
                      'lib/lake.jpg',
                      height: 240.0,
                      fit: BoxFit.cover,
                    ),
                  ),
                  new Text(
                    'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                    style: new TextStyle(
                      color: Colors.grey[500],
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
